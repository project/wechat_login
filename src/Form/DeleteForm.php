<?php

namespace Drupal\wechat_login\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides a form for deleting third party user entities.
 */
class DeleteForm extends ContentEntityConfirmFormBase
{

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete entity %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   * @throws EntityMalformedException
   */
  public function getCancelUrl() {
    return $this->getEntity()->toUrl('canonical');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    try {
      $this->entity->delete();
    } catch (EntityStorageException $e) {
      watchdog_exception('wechat_login', $e);
      \Drupal::logger('wechat_login')->error($this->t('Authentication failed, @message. Please try again.', ['@message' => $e->getMessage()]));
      // Redirect to homepage on failure
      return new RedirectResponse(Url::fromUri('internal:/')->toString());
    }

    \Drupal::logger('wechat_login')->error($this->t('content @type: deleted @label.',
      [
        '@type' => $this->entity->bundle(),
        '@label' => $this->entity->label()
      ]));


    $form_state->setRedirectUrl(new Url('entity.third_party_user.collection'));
  }

}
