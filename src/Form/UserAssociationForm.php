<?php

namespace Drupal\wechat_login\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * TODO: class docs.
 */
class UserAssociationForm extends FormBase
{

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'open_connect_custom_user_association_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form['email'] = [
      '#type' => "email",
      '#title' => $this->t("Email"),
    ];
    $form['name'] = [
      '#type' => "textfield",
      '#title' => $this->t("Username"),
      '#maxlength' => UserInterface::USERNAME_MAX_LENGTH,
      '#required' => TRUE,
      '#attributes' => [
        'class' => ['username'],
        'autocorrect' => 'off',
        'autocapitalize' => 'off',
        'spellcheck' => 'false',
      ],
    ];
    $form['password'] = [
      '#type' => 'password_confirm',
      '#title' => $this->t("Password"),
      '#size' => 25,
      '#description' => $this->t('To change the current user password, enter the new password in both fields.'),
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $email = $form_state->getValue('email');
    $name = $form_state->getValue('name');
    $password = $form_state->getValue('password');
    $associated_uid = \Drupal::service('user.auth')->authenticate($name, $password);
    if ($associated_uid) {
      $associated_user = User::load($associated_uid);
      $user = \Drupal::routeMatch()->getParameter('user');
      $storage = \Drupal::service('entity_type.manager')->getStorage('open_connect');
      $open_connect_entities = $storage->loadMultipleByUser($user);
      $open_connect_entity = reset($open_connect_entities);
      $open_connect_entity->setAccount($associated_user);
      $open_connect_entity->save();
      $user->delete();
      user_login_finalize($associated_user);
      $form_state->setRedirectUrl(Url::fromUri('internal:/user'));
    }
  }

}
