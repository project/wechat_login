<?php

namespace Drupal\wechat_login\Form;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\wechat_login\Controller\RedirectController;
use Drupal\wechat_login\Plugin\ThirdpartyUserLogin\Provider\ProviderInterface;
use Drupal\wechat_login\Plugin\ThirdpartyUserLogin\ProviderManagerInterface;
use Drupal\wechat_login\UncacheableTrustedRedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

class LoginForm extends FormBase implements ContainerInjectionInterface {

  /**
   * The plugin manager.
   *
   * @var ProviderManagerInterface
   */
  protected ProviderManagerInterface $pluginManager;

  /**
   * The request stack.
   *
   * @var RequestStack
   */
  protected $requestStack;

  /**
   * The CSRF token generator.
   *
   * @var CsrfTokenGenerator
   */
  protected CsrfTokenGenerator $csrfToken;

  /**
   * Constructs a new LoginForm object.
   *
   * @param ProviderManagerInterface $plugin_manager
   *   The plugin manager.
   * @param RequestStack $request_stack
   *   The request stack.
   * @param CsrfTokenGenerator $csrf_token
   *   The CSRF token generator.
   */
  public function __construct(ProviderManagerInterface $plugin_manager, RequestStack $request_stack, CsrfTokenGenerator $csrf_token) {
    $this->pluginManager = $plugin_manager;
    $this->requestStack = $request_stack;
    $this->csrfToken = $csrf_token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.third_party_user_login.provider'),
      $container->get('request_stack'),
      $container->get('csrf_token')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wechat_login_login_form';
  }

  /**
   * {@inheritdoc}
   *
   * @todo: WeChat MP should only be available on WeChat Client.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $plugins = $this->pluginManager->getDefinitions();
    $config = $this->config('wechat_login.settings');
    $enabled_providers = $config->get('providers');
    foreach ($plugins as $id => $definition) {
      if (!isset($enabled_providers[$id])) {
        continue;
      }
      $moduleHandler = \Drupal::service('module_handler');
      if ($moduleHandler->moduleExists('mobile_detect')) {
        $md = \Drupal::service('mobile_detect');
        $is_mobile = $md->isMobile();
        $is_tablet = $md->isTablet();
        if ($id == "wechat_mp") {
          if ($is_mobile || $is_tablet) {
            $form['wechat_login_login_' . $id] = [
              '#type' => 'submit',
              '#value' => $this->t('Log in with @label', ['@label' => $definition['label']]),
              '#name' => $id,
              '#prefix' => '<div class="wechat-login">',
              '#suffix' => '</div>',
            ];
          }
          continue;
        }
        if($id == "wechat_op"){
          if (!$is_mobile && !$is_tablet) {
            $form['wechat_login_login_' . $id] = [
              '#type' => 'submit',
              '#value' => $this->t('Log in with @label', ['@label' => $definition['label']]),
              '#name' => $id,
              '#prefix' => '<div class="wechat-login">',
              '#suffix' => '</div>',
            ];
          }
        }
      }
      else {
        $form['wechat_login_login_' . $id] = [
          '#type' => 'submit',
          '#value' => $this->t('Log in with @label', ['@label' => $definition['label']]),
          '#name' => $id,
          '#prefix' => '<div class="wechat-login">',
          '#suffix' => '</div>',
        ];
      }
    }
    $form['#attached']['library'][] = 'wechat_login/login_block';
    $form['#cache']['max-age'] = 0;
    CacheableMetadata::createFromRenderArray($form)
      ->addCacheableDependency($config)
      ->applyTo($form);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $id = $form_state->getTriggeringElement()['#name'];
    $enabled_providers = $this->config('wechat_login.settings')
      ->get('providers');
    /** @var ProviderInterface $provider */
    try {
      $provider = $this->pluginManager->createInstance($id, $enabled_providers[$id]);
    } catch (PluginException $e) {
      watchdog_exception('wechat_login', $e);
      \Drupal::logger('wechat_login')
        ->error($this->t('Authentication failed, @message. Please try again.', ['@message' => $e->getMessage()]));
      // Redirect to homepage on failure
      // @todo: Handle external path.
      return new RedirectResponse(Url::fromUri('internal:/')->toString());
    }

    $request = $this->requestStack->getCurrentRequest();
    // Save something in the session attribute bag, or the session could not be
    // persisted, see SessionManager::save().
    // By settings values in the session bag, the session will be automatically
    // started for the anonymous user.
    $configuration = $request->getSession()->get('wechat_login', []);
    $configuration['operation'] = 'login';
    $configuration['return_uri'] = wechat_login_get_return_uri($request->getRequestUri());
    $request->getSession()->set('wechat_login', $configuration);

    $token_key = $this->config('wechat_login.settings')->get('token_key');
    $state = $this->csrfToken->get($token_key);
    $url = $provider->getAuthorizeUrl($state)->toString();
    // Unreachable because the response depends on a dynamic crsf token.
    $form_state->setResponse(new UncacheableTrustedRedirectResponse($url));
  }

}
