<?php

namespace Drupal\wechat_login;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\wechat_login\Entity\ThirdpartyUserInterface;

/**
 * Defines an interface for node entity storage classes.
 */
interface ThirdpartyUserStorageInterface extends ContentEntityStorageInterface {

  /**
   * Loads the third party user for the given provider and openid.
   *
   * @param string $provider
   *   The identity provider which the openid belongs to.
   * @param string $openid
   *   The openid.
   *
   * @return ThirdpartyUserInterface|null
   *   The third party user, or NULL if none found.
   */
  public function loadByOpenid(string $provider, string $openid);

  /**
   * Loads the third party user for the given provider and openid.
   *
   * @param string $unionid
   *   The unionid.
   *
   * @return ThirdpartyUserInterface|null
   *   The third party user, or NULL if none found.
   */
  public function loadByUnionid(string $unionid);

  /**
   * Loads the third party user for the given user.
   *
   * @param AccountInterface $account
   *    The user entity.
   * @param string|null $provider
   *   (Optional) The identity provider.
   *
   * @return ThirdpartyUserInterface[]
   *   The third party user entities.
   */
  public function loadMultipleByUser(AccountInterface $account, string $provider = NULL);

}
