<?php

namespace Drupal\wechat_login;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the storage handler class for third party user.
 *
 * This extends the base storage class, adding required special handling for
 * third party user entities.
 */
class ThirdpartyUserStorage extends SqlContentEntityStorage implements ThirdpartyUserStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByOpenid(string $provider, string $openid) {
    $entities = $this->loadByProperties([
      'provider' => $provider,
      'openid' => $openid,
    ]);
    $entity = reset($entities);

    return $entity ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadByUnionid(string $unionid) {
    $entities = $this->loadByProperties(['unionid' => $unionid]);
    $entity = reset($entities);

    return $entity ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultipleByUser(AccountInterface $account, string $provider = NULL) {
    $query = $this->getQuery()
      ->condition('uid', $account->id())
      ->sort('provider');
    if ($provider) {
      $query->condition('provider', $provider);
    }
    $result = $query->execute();

    return $result ? $this->loadMultiple($result) : [];
  }

}
