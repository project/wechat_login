<?php

namespace Drupal\wechat_login\Plugin\ThirdpartyUserLogin\Provider;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Url;
use Drupal\wechat_login\Exception\ThirdpartyUserLoginException;
use Drupal\user\UserInterface;

/**
 * Creates an interface for identity providers.
 */
interface ProviderInterface extends PluginFormInterface, ConfigurableInterface, PluginInspectionInterface {

  /**
   * Gets to authorize url.
   *
   * @param string $state
   *    The state query parameter to prevent CSRF.
   *
   * @return Url
   *   To authorize url.
   */
  public function getAuthorizeUrl(string $state);

  /**
   * Authenticates a user with the given code.
   *
   * @param string $code
   *   The authorization code.
   *
   * @return UserInterface
   *   The authenticated user.
   *
   * @throws ThirdpartyUserLoginException
   *   Thrown when authentication fails for any reason.
   */
  public function authenticate(string $code);

}
