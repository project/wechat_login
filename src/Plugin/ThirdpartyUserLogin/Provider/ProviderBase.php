<?php

namespace Drupal\wechat_login\Plugin\ThirdpartyUserLogin\Provider;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Url;
use Drupal\wechat_login\Exception\ThirdpartyUserLoginException;
use Drupal\user\UserInterface;
use Drupal\wechat_login\ThirdpartyUserStorageInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class ProviderBase extends PluginBase implements ProviderInterface, ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The http client.
   *
   * @var Client
   */
  protected Client $httpClient;

  /**
   * The language manager.
   *
   * @var LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The logger.
   *
   * @var LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Constructs a new ProviderBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param Client $http_client
   *   The http client object.
   * @param LanguageManagerInterface $language_manager
   *   The language manager.
   * @param LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, Client $http_client, LanguageManagerInterface $language_manager, LoggerChannelFactoryInterface $logger_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->httpClient = $http_client;
    $this->languageManager = $language_manager;
    $this->logger = $logger_factory->get('wechat_login');
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('http_client'),
      $container->get('language_manager'),
      $container->get('logger.factory')
    );
  }

  public function calculateDependencies() {
    // TODO: dependencies
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'mode' => 'test',
      'client_id' => '',
      'client_secret' => '',
      'scope' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep($this->defaultConfiguration(), $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Mode'),
      '#options' => [
        'test' => t('Test'),
        'live' => t('Live'),
      ],
      '#default_value' => $this->configuration['mode'],
    ];
    $form['client_id'] = [
      '#title' => $this->t('Client ID'),
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#default_value' => $this->configuration['client_id'],
    ];
    $form['client_secret'] = [
      '#title' => $this->t('Client Secret'),
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#default_value' => $this->configuration['client_secret'],
    ];
    $form['scope'] = [
      '#title' => $this->t('Scope'),
      '#type' => 'textfield',
      '#maxlength' => 255,
      '#default_value' => $this->configuration['scope'],
    ];
    $form['redirect_uri'] = [
      '#title' => $this->t('Redirect URI'),
      '#type' => 'item',
      '#input' => FALSE,
      '#markup' => $this->getRedirectUri(),
    ];
    if ($homepage = $this->pluginDefinition['homepage']) {
      $params = [
        '@homepage' => $homepage,
        '@description' => $this->pluginDefinition['description'],
      ];
      $form['description'] = [
        '#markup' => '<div class="description">' . $this->t('Set up your app on <a href="@homepage" target="_blank">@description</a>.', $params) . '</div>',
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (empty($values['client_id'])) {
      $form_state->setError($form['client_id'], 'Client ID cannot be empty.');
    }
    if (empty($values['client_secret'])) {
      $form_state->setError($form['client_secret'], 'Client secret cannot be empty.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configuration['mode'] = $values['mode'];
    $this->configuration['client_id'] = $values['client_id'];
    $this->configuration['client_secret'] = $values['client_secret'];
    $this->configuration['scope'] = $values['scope'];
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthorizeUrl(string $state) {
    $this->assertConfiguration();

    $options = [
      'query' => [
        'response_type' => 'code',
        $this->getKey('client_id') => $this->configuration['client_id'],
        'redirect_uri' => $this->getRedirectUri(),
        'state' => $state,
        'scope' => $this->configuration['scope'],
      ],
    ];
    $options['query'] = array_filter($options['query']);
    $this->processRedirectUrlOptions($options);
    return Url::fromUri($this->getUrl('authorization'), $options);
  }

  /**
   * Processes the redirect url options.
   *
   * @param array $options
   *   The url options.
   */
  protected function processRedirectUrlOptions(array &$options) {
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate(string $code) {
    $this->assertConfiguration();
    $this->logResponse('Authorization code', ['code' => $code]);

    // Fetch authorization token.
    $token_response = $this->fetchToken($code);

    // Get openid from the token response or fetch it if not found.
    // Providers like WeChat and Weibo return an openid in the token response.
    $openid_key = $this->getKey('openid');
    $access_token = isset($token_response[$openid_key]) ? $token_response[$openid_key] : $token_response['access_token'];
    $openid = $access_token;
    // WeChat's providers may return an unionid.
    $unionid = isset($token_response['unionid']) ? $token_response['unionid'] : '';

    /** @var ThirdpartyUserStorageInterface $third_party_user_storage */
    $third_party_user_storage = $this->entityTypeManager->getStorage('third_party_user');
    $third_party_user = $third_party_user_storage->loadByOpenid($this->pluginId, $openid);
    if (!$third_party_user) {
      $independent_account = FALSE;
      if (in_array($this->getPluginId(), ['wechat_mp', 'wechat_op'])) {
        $independent_account = $this->getKey('independent_account');
      }
      // Try to load by unionid.
      if (!$independent_account && $unionid && $third_party_user = $third_party_user_storage->loadByUnionid($unionid)) {
        // Get the existing user.
        $user = $third_party_user->getAccount();
      }
      else {
        // Create a new user to authenticate.
        $user = $this->createUser();
      }

      // Create an third party user entity with the new provider, openid and
      // possible unionid for the existing user or a new user.
      $third_party_user = $third_party_user_storage->create([
        'provider' => $this->pluginId,
        'openid' => $openid,
        'unionid' => $unionid,
        'uid' => $user->id(),
      ]);
      try {
        $third_party_user->save();
      } catch (EntityStorageException $e) {
        watchdog_exception('wechat_login', $e);
        \Drupal::logger('wechat_login')
          ->error($this->t('Authentication failed, @message. Please try again.', ['@message' => $e->getMessage()]));
      }
    }
    elseif ($unionid && !$third_party_user->getUnionid()) {
      // Save the unionid.
      try {
        $third_party_user->setUnionid($unionid)->save();
      } catch (EntityStorageException $e) {
        watchdog_exception('wechat_login', $e);
        \Drupal::logger('wechat_login')
          ->error($this->t('Authentication failed, @message. Please try again.', ['@message' => $e->getMessage()]));
      }
    }

    return $third_party_user->getAccount();
  }

  /**
   * Configuration assertion.
   */
  private function assertConfiguration() {
    if (empty($this->configuration['client_id'])) {
      throw new \InvalidArgumentException('Client ID is not set.');
    }
    if (empty($this->configuration['client_secret'])) {
      throw new \InvalidArgumentException('Client secret is not set.');
    }
  }

  /**
   * Fetches an authorization token by the given authorization code.
   *
   * @param string $code
   *   The authorization code.
   *
   * @return array
   *   An array of response data values.
   *
   * @throws ThirdpartyUserLoginException
   *   Thrown when the http request fails or the response is failed.
   */
  protected function fetchToken(string $code) {
    try {
      // Fetch authorization token.
      $token_response = $this->doFetchToken($this->getUrl('access_token'), [
        'grant_type' => 'authorization_code',
        'code' => $code,
        'redirect_uri' => $this->getRedirectUri(),
        $this->getKey('client_id') => $this->configuration['client_id'],
        $this->getKey('client_secret') => $this->configuration['client_secret'],
      ]);
      $this->logResponse('Fetch token', $token_response);
    } catch (RequestException $e) {
      throw new ThirdpartyUserLoginException(sprintf('%s Could not fetch authorization token: %s', $this->pluginId, $e->getMessage()), $e->getCode(), $e);
    }

    if (!$this->isResponseSuccessful($token_response)) {
      throw new ThirdpartyUserLoginException($this->getResponseError($token_response));
    }

    return $token_response;
  }

  /**
   * Preforms a provider-specific fetching token.
   *
   * @param string $url
   *   The api url.
   * @param array $params
   *   The request parameters.
   *
   * @return array
   *   The token array
   *
   * @throws RequestException
   *   Thrown when the http request fails.
   */
  protected function doFetchToken(string $url, array $params) {
    $response = $this->httpClient->post($url, [
      'form_params' => $params,
    ]);

    // getBody() returns an instance of Psr\Http\Message\StreamInterface.
    // @see http://docs.guzzlephp.org/en/latest/psr7.html#body
    return \GuzzleHttp\json_decode($response->getBody(), TRUE);
  }

  /**
   * Fetches openid.
   *
   * @param string $access_token
   *   The access token.
   *
   * @return string
   *   The openid.
   *
   * @throws ThirdpartyUserLoginException
   *   Thrown when the http request fails or the response is failed.
   */
  protected function fetchOpenid(string $access_token) {
    try {
      $openid_response = $this->doFetchOpenid($this->getUrl('openid'), [
        'access_token' => $access_token,
      ]);
      $this->logResponse('Fetch openid', $openid_response);
    } catch (RequestException $e) {
      throw new ThirdpartyUserLoginException(sprintf('%s Could not fetch openid: %s', $this->pluginId, $e->getMessage()), $e->getCode(), $e);
    }

    // Throws when the transaction fails for any reason, see SupportsRefundsInterface.
    if (!$this->isResponseSuccessful($openid_response)) {
      throw new ThirdpartyUserLoginException($this->getResponseError($openid_response));
    }

    $openid_key = $this->getKey('openid');
    return $openid_response[$openid_key];
  }

  /**
   * Preforms a provider-specific fetching openid.
   *
   * @param string $url
   *   The api url.
   * @param array $params
   *   The request parameters.
   *
   * @return array
   *   An array of response data values.
   *
   * @throws RequestException
   *   Thrown when the http request fails.
   */
  protected function doFetchOpenid(string $url, array $params) {
    $response = $this->httpClient->get($url, [
      'query' => $params,
    ]);
    return \GuzzleHttp\json_decode($response->getBody(), TRUE);
  }

  /**
   * Fetches the user info with the given access token and openid.
   *
   * @param string $access_token
   *   The access token.
   * @param string $openid
   *   The openid.
   *
   * @return array
   *   The user claims.
   *
   * @throws ThirdpartyUserLoginException
   *   Thrown when the http request fails or the response is failed.
   */
  public function fetchUserInfo(string $access_token, string $openid) {
    $openid_key = $this->getKey('openid');
    $params = [
      'access_token' => $access_token,
      $openid_key => $openid,
    ];
    return $this->doFetchUserInfo($this->getUrl('user_info'), $params);
  }

  /**
   * Preforms a provider-specific fetching token.
   *
   * @param string $url
   *   The api url.
   * @param array $params
   *   The request parameters.
   *
   * @return array
   *   The token array
   *
   * @throws RequestException
   *   Thrown when the http request fails.
   */
  protected function doFetchUserInfo(string $url, array $params) {
    $response = $this->httpClient->get($url, [
      'query' => $params,
    ]);
    return \GuzzleHttp\json_decode($response->getBody(), TRUE);
  }

  /**
   * Creates a new user.
   *
   * @return UserInterface
   *   The newly created user object.
   */
  private function createUser() {
    try {
      $user_storage = $this->entityTypeManager->getStorage('user');
    } catch (InvalidPluginDefinitionException $e) {
    } catch (PluginNotFoundException $e) {
      watchdog_exception('wechat_login', $e);
      \Drupal::logger('wechat_login')
        ->error($this->t('Authentication failed, @message. Please try again.', ['@message' => $e->getMessage()]));
    }

    // Get a unique username.
    $name = 'u' . date('YmdHis');
    $i = 0;
    while ($user_storage->loadByProperties(['name' => $name])) {
      $name .= '_' . ++$i;
    }

    /** @var UserInterface $user */
    $user = $user_storage->create([
      'name' => $name,
      'pass' => \Drupal::service('password_generator')->generate(),
      'mail' => '',
    ]);
    // Always active the new user.
    $user->activate();
    try {
      $user->save();
    } catch (EntityStorageException $e) {
      watchdog_exception('wechat_login', $e);
      \Drupal::logger('wechat_login')
        ->error($this->t('Authentication failed, @message. Please try again.', ['@message' => $e->getMessage()]));
    }

    return $user;
  }

  /**
   * Gets a url for the given api.
   *
   * @param string $api
   *   The api.
   *
   * @return string|bool
   *   The url, or FALSE if not found.
   */
  protected function getUrl(string $api) {
    $urls = $this->pluginDefinition['urls'];
    return isset($urls[$api]) ? $urls[$api] : FALSE;
  }

  /**
   * Gets a specific key.
   *
   * @param string $key
   *   The name of the key to return.
   *
   * @return string|bool
   *   The key, or FALSE if it does not exist.
   */
  protected function getKey(string $key) {
    $keys = $this->pluginDefinition['keys'];
    return isset($keys[$key]) ? $keys[$key] : FALSE;
  }

  /**
   * Gets the redirect uri.
   *
   * @return string
   */
  private function getRedirectUri() {
    // Redirect uri
    return Url::fromRoute('wechat_login.authenticate', [
      'third_party_user_login_provider' => $this->pluginId,
    ], [
      // 'query' => $redirect_uri_query,
      'absolute' => TRUE,
      'language' => $this->languageManager->getLanguage(LanguageInterface::LANGCODE_NOT_APPLICABLE),
    ])->toString();
  }

  /**
   * Logs response.
   *
   * @param string $operation
   *   The operation
   * @param array $response
   *   An array of response data.
   */
  private function logResponse(string $operation, array $response) {
    if (empty($response)) {
      return;
    }
    // Log response if the data represents an failure, or the plugin is not in
    // live mode.
    $successful = $this->isResponseSuccessful($response);
    if (!$successful || $this->configuration['mode'] !== 'live') {
      $level = $successful ? 'debug' : 'warning';
      $this->logger->$level('@provider @operation: <pre>@response</pre>', [
        '@provider' => $this->pluginId,
        '@operation' => $operation,
        '@response' => print_r($response, TRUE),
      ]);
    }
  }

  /**
   * Whether the response represents a successful data.
   *
   * @param array $response
   *   The response data.
   *
   * @return bool
   *   True if the existing payment is available for reuse, FALSE otherwise.
   */
  abstract protected function isResponseSuccessful(array $response);

  /**
   * Gets the error message from the given response data.
   *
   * @param array $response
   *   The response data
   *
   * @return string
   *   The error messages.
   */
  abstract protected function getResponseError(array $response);

}
