<?php

namespace Drupal\wechat_login\Plugin\ThirdpartyUserLogin;

use Drupal\Component\Plugin\PluginManagerInterface;

interface ProviderManagerInterface extends PluginManagerInterface {

}
