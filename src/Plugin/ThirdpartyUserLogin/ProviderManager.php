<?php

namespace Drupal\wechat_login\Plugin\ThirdpartyUserLogin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

class ProviderManager extends DefaultPluginManager implements ProviderManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ThirdpartyUserLogin/Provider', $namespaces, $module_handler, 'Drupal\wechat_login\Plugin\ThirdpartyUserLogin\Provider\ProviderInterface', 'Drupal\wechat_login\Annotation\ThirdpartyUserLoginProvider');
    $this->setCacheBackend($cache_backend, 'third_party_user_login_provider');
    $this->alterInfo('third_party_user_login_provider_info');
  }

}
