<?php

namespace Drupal\wechat_login\Controller;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\wechat_login\Exception\ThirdpartyUserLoginException;
use Drupal\wechat_login\Plugin\ThirdpartyUserLogin\Provider\ProviderInterface;
use Drupal\wechat_login\Plugin\ThirdpartyUserLogin\ProviderManager;
use Drupal\wechat_login\UncacheableTrustedRedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RedirectController extends ControllerBase implements AccessInterface {

  /**
   * Drupal\wechat_login\Plugin\ThirdpartyUserLogin\ProviderManager definition.
   *
   * @var ProviderManager
   */
  protected ProviderManager $pluginManager;

  /**
   * The renderer.
   *
   * @var RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The request stack used to access request globals.
   *
   * @var RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * The CSRF token generator.
   *
   * @var CsrfTokenGenerator
   */
  protected CsrfTokenGenerator $csrfToken;

  /**
   * Constructs a new RedirectController instance.
   *
   * @param ProviderManager $plugin_manager
   *   The identity provider manager.
   * @param RendererInterface $renderer
   *   The renderer.
   * @param RequestStack $request_stack
   *   The request stack.
   * @param CsrfTokenGenerator $csrf_token
   *   The csrf token.
   */
  public function __construct(
    ProviderManager    $plugin_manager,
    RendererInterface  $renderer,
    RequestStack       $request_stack,
    CsrfTokenGenerator $csrf_token
  ) {
    $this->pluginManager = $plugin_manager;
    $this->renderer = $renderer;
    $this->requestStack = $request_stack;
    $this->csrfToken = $csrf_token;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.third_party_user_login.provider'),
      $container->get('renderer'),
      $container->get('request_stack'),
      $container->get('csrf_token')
    );
  }

  /**
   * Checks access for the authentication callback.
   *
   * @param AccountInterface $account
   *   The current user account.
   *
   * @see \Drupal\Core\Access\CustomAccessCheck::access()
   */
  public function checkAccess(AccountInterface $account) {
    $request = $this->requestStack->getCurrentRequest();
    // Confirm anti-forgery state token. This round-trip verification helps to
    // ensure that the user, not a malicious script, is making the request.
    $token_key = $this->config('wechat_login.settings')->get('token_key');
    if ($this->csrfToken->validate($request->query->get('state', ''), $token_key)) {
      // Check operation.
      $configuration = $request->getSession()->get('wechat_login', []);
      $configuration += ['operation' => 'login'];
      if ($configuration['operation'] === 'login' xor $account->isAuthenticated()) {
        // 'login' for anonymous user or 'connect' for authenticated user.
        $result = AccessResult::allowed();
      }
      else {
        $result = AccessResult::forbidden($configuration['operation'] === 'login' ?
          'Only anonymous user can log in with Wechat Login.' :
          'Ensure the user is logged in.'
        );
      }
    }
    else {
      // Invalid state parameter.
      $result = AccessResult::forbidden($request->query->has('state') ?
        "The 'state' query argument is invalid." :
        "The 'state' query argument is missing."
      );
    }

    // Uncacheable because the CSRF token is highly dynamic.
    return $result->setCacheMaxAge(0);
  }

  /**
   * Authorize by redirecting to an external url.
   *
   * @param $third_party_user_login_provider
   *   The provider id.
   * @param Request $request
   *   Current request object.
   *
   * @return UncacheableTrustedRedirectResponse
   *   The redirect response.
   * @throws PluginException
   */
  public function authorize($third_party_user_login_provider, Request $request) {
    $enabled_providers = $this->config('wechat_login.settings')
      ->get('providers');
    if (empty($enabled_providers[$third_party_user_login_provider])) {
      throw new BadRequestHttpException('Invalid identity provider.');
    }

    // Set wechat_login configuration.
    $configuration = $request->getSession()->get('wechat_login', []);
    $configuration += [
      'operation' => 'login',
      'return_uri' => $request->query->get('return_uri', '/user'),
    ];
    $request->getSession()->set('wechat_login', $configuration);

    /** @var ProviderInterface $provider */
    $provider = $this->pluginManager->createInstance($third_party_user_login_provider, $enabled_providers[$third_party_user_login_provider]);
    $token_key = $this->config('wechat_login.settings')->get('token_key');
    $state = $this->csrfToken->get($token_key);

    $url = $provider->getAuthorizeUrl($state)->toString();
    // Unreachable because the response depends on a dynamic crsf token.
    return new UncacheableTrustedRedirectResponse($url);
  }

  /**
   * Authenticate user.
   *
   * @param string $third_party_user_login_provider
   *   The provider plugin id.
   * @param Request $request
   *   Current request object.
   *
   * @return RedirectResponse|UncacheableTrustedRedirectResponse The redirect
   *   response. The redirect response.
   */
  public function authenticate(string $third_party_user_login_provider, Request $request) {
    if (!$code = $request->query->get('code')) {
      // The URI is probably being visited outside the login flow.
      throw new NotFoundHttpException();
    }

    $enabled_providers = $this->config('wechat_login.settings')
      ->get('providers');
    if (empty($enabled_providers[$third_party_user_login_provider])) {
      throw new BadRequestHttpException('Invalid identity provider.');
    }

    $configuration = $request->getSession()->get('wechat_login', [
      'operation' => 'login',
      'return_uri' => '/user',
    ]);
    // Delete the configuration, since it's already been consumed.
    $request->getSession()->remove('wechat_login');

    /** @var ProviderInterface $provider */
    try {
      $provider = $this->pluginManager->createInstance($third_party_user_login_provider, $enabled_providers[$third_party_user_login_provider]);
    } catch (PluginException $e) {
      watchdog_exception('wechat_login', $e);
      \Drupal::logger('wechat_login')
        ->error($this->t('Authentication failed, @message. Please try again.', ['@message' => $e->getMessage()]));
      // Redirect to homepage on failure
      // @todo: Handle external path.
      return new RedirectResponse(Url::fromUri('internal:/')->toString());
    }

    try {
      // Authenticate a user with the code.
      $user = $provider->authenticate($code);
    } catch (ThirdpartyUserLoginException $e) {
      watchdog_exception('wechat_login', $e);
      \Drupal::logger('wechat_login')
        ->error($this->t('Authentication failed, @message. Please try again.', ['@message' => $e->getMessage()]));
      // Redirect to homepage on failure
      // @todo: Handle external path.
      return new RedirectResponse(Url::fromUri('internal:/')->toString());
    }

    if (!$user->isActive()) {
      throw new AccessDeniedHttpException('The user is blocked.');
    }
    if ($configuration['operation'] === 'login') {
      user_login_finalize($user);
    }
    if (UrlHelper::isExternal($configuration['return_uri'])) {
      // Uncacheable because the response is following authorize().
      return new UncacheableTrustedRedirectResponse($configuration['return_uri']);
    }
    return new RedirectResponse(Url::fromUri('internal:' . $configuration['return_uri'])
      ->toString());
  }

}
